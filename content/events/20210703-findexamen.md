+++
title = "Fin d'Examen!"
date = "2021-07-03T21:00:00+0100"
location = "JK ZOMAAR, Keperenbergstraat 37, 1701 Itterbeek"
description = "Terras fuif"
prijs = "Gratis"
+++
![banner](https://scontent-bru2-1.xx.fbcdn.net/v/t1.6435-9/202538916_10159260742022597_444958124537938248_n.jpg?_nc_cat=110&ccb=1-3&_nc_sid=340051&_nc_ohc=u1EehsSzFloAX9E0mm1&_nc_ht=scontent-bru2-1.xx&oh=262682c91b8837768f90dc1d3918d75c&oe=60E22A02)

😌 Eindelijk gedaan!

🍻 Goede resultaten moeten gevierd worden, minder goede weg gedronken!

💿 Vanaf 17u komen er enkele DJs plaatjes draaien! (TBA)

😷 Uiteraard worden de corona maatregelen volledig nageleefd, en gaat dit al zittend door. Tafels van max 8.

Reserveren hoeft niet, maar vol = vol!

[Facebook Event](https://www.facebook.com/events/178105447619937)
