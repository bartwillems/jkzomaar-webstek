+++
title = "Info"
date = "2021-05-21"
+++

## Adres

{{< partial "info/map" >}}

Keperenbergstraat 37

1701 Itterbeek

## Openingsuren

Elke vrijdag vanaf 20u baravond.

Andere evenementen kan je vinden op onze [events](/events) pagina!

## Betalingsmogelijkheden

- bankkaart/kredietkaart
- zuip-of-kruipkaart
- cash (liefst zo weinig mogelijk ivm de coronamaatregelen)

## Dranklijst

| Alcoholische Dranken | Prijs(€) |
| :------------------- | -------: |
| Stella               |     1.50 |
| Vedett               |     2.00 |
| Duvel                |     2.50 |
| Duvel Tripel Hop     |     2.50 |
| Tripel Karmeliet     |     2.50 |
| La Trappe Quadrupel  |     2.50 |
| Geuze Boon           |     2.00 |
| Kriek Lindemans      |     1.50 |
| Timmermans Kriek     |     1.50 |
| Timmermans Pecheresse|     1.50 |
| Rougeke              |     2.50 |
| Cava                 |     2.00 |
| Wijn (rood/rosé/wit) |     2.00 |


| Speciallekes (Beperkt)                |    Prijs(€) |
| :------------------------------------ | ----------: |
| Cantillon Geuze 37cl                  |        5.00 |
| Girardin Geuze 37cl                   |        5.00 |
| Lindemans Geuze 25cl                  |        2.00 |
| Vandervelden Geuze 37cl               |        6.00 |


| Non-Alcoholische Dranken        | Prijs(€) |
| :-------------------------- | -------: |
| Kraantjeswater              |   Gratis |
| Water uit fles (Plat/Bruis) |     1.00 |
| Coca Cola Zero              |     1.50 |
| Iced Tea (Normaal/Green)    |     1.50 |
| Orangina                    |     1.50 |
| Ritchie Cola                |     1.50 |
| Ritchie Lemon               |     1.50 |
| Koffie                      |     1.50 |
| Sportzot                    |     2.00 |

| Eten         | Prijs(€) |
| :----------- | -------: |
| Chips        |     1.00 |
| Aïki Noodles |     2.50 |
| Royco Soep   |     1.50 |
| Royco Pasta  |     2.50 |
