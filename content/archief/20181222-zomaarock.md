+++
title = "ZomaaRock: Forge, Jerk Beefy"
date = "2018-12-22"
+++

![banner](/img/20181222-zomaarock-banner.jpg)

>FORGE: https://www.facebook.com/forgepunk/
>
>JERK BEEFY: https://www.facebook.com/jerkbeefyband/
>
>Gratis inkom voor leden van JK Zomaar!
>
>2 Euro voor niet-leden.
>
>(Lid worden kan ter plaatse voor slechts 5 euro, profijtelijk!)
>
>Doors om 20u, de eerste band rond 20u30.

{{< album 20181222-zomaarock >}}

bron: [Facebook](https://www.facebook.com/events/338753970035915/)
