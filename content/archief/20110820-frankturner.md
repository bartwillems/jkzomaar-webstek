+++
title = "Optreden Frank Turner"
date = "2011-08-20"
+++

Frank Turner tijdens het gratis optreden in JK Zomaar in Itterbeek dat hij gaf in plaats van het optreden die avond op Pukkelpop 2011.

Gedeeltelijke setlist:

*  I Knew Prufrock Before He Got Famous
*  I Still Believe
*  Substitute
*  Peggy Sang the Blues
*  Photosynthesis 


{{< youtube oZgwVbjZlDs >}}  

bron: [Setlist.fm](https://www.setlist.fm/setlist/frank-turner/2011/jk-zomaar-itterbeek-belgium-7bc6b210.html)
