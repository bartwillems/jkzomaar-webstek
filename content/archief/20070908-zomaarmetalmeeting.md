+++
title = "Zomaar Metal Meeting"
date = "2007-09-08"
+++
>Het festival bestaat uit:
>
>* Een grote witte tent waar de optredens in plaatsvinden.
>
>* Eén of twee eetstandjes waar hamburgers, frieten, hot dogs en dergelijke te koop zijn.
>
>* Een zwaar bieren tent.
>
>* Een tap voor bier, frisdranken en kleine versnaperingen.
>
>* Voldoende tafeltjes en stoelen.
>
>* Een tent waar de merchandise van de aanwezige bands wordt aangeboden.
>
>* Een EHBO-tent.
>
>De prijs in vvk bedraagt 5 euro en aan de kassa zal de prijs 8 euro bedragen. vvk kaarten kunnen besteld worden op zomaarmetal@hotmail.com. Het festival gaat door op 8 september en het begint om 14uur en zal beëindigt worden om 2 uur 's nachts. 

![poster](https://web.archive.org/web/20071029024510if_/http://www.zomaarmetal.be/images/flyer.jpg)

{{< youtube hIbRDAcqYBo >}}

{{< youtube 7rL22jzxr8w >}}

bron: [festivalinfo.nl](https://www.festivalinfo.nl/festival/6822/Zomaar_Metal_Meeting/2007/), [zomaarmetal.be op archive.org](https://web.archive.org/web/20080430143849/http://www.zomaarmetal.be/)
