+++
title = "New Year's Party 2019"
date = "2018-12-31"
+++
![banner](/img/20181231-nye.jpg)

>Beste vrienden van de Zomaar, 
>
>Binnenkort maakt 2018 plaats voor een nieuw jaar. 2019 is near! 
>
>Zin om te vieren? Je hoeft het niet ver te zoeken...
>
>Op de New Years Party van JK Zomaar vind je alle fun, met het extraatje dat het dicht bij huis is. Twijfel niet en komt de overgang van 2018 naar 2019 bij ons vieren! 
>
>
>♫ Vol trots presenteren wij dan ook de klassebakken die onze NYE van muziek komen voorzien: 
>
> ► 23:00 - 00:30 Space Toad
>
> ► 00:30 - 01:30 DJ Mazze
>
> ► 01:30 - 03:30 Triple Noise

{{< album 20181231-nye >}}
bron: [Facebook](https://www.facebook.com/events/305601636950083/)
