+++
title = "FAQ"
date = "2019-09-09"
+++

### Wanneer zijn jullie open?
~~Elke vrijdag vanaf 20u!~~ Onze feestjes vind je [hier](../events/).

### Wat kan ik er drinken?
Onze drankenlijst vind je [hier](../info/#dranklijst).

### Wat is er te doen op een baravond?
Buiten komen toogplakken en genieten van de grootste bierkaart van menig Dilbeeks jeugdhuis (door al dan niet aan het Rad van Dorstuin te draaien), kan je op onze kickertafel komen spelen, vogelepik spelen, of 1 van onze gezelschapsspelen. Je mag er natuurlijk ook steeds zelf meebrengen! Er wordt ook regelmatig bierpong gespeeld, of een poging gedaan om één of ander record te breken.

### Kan ik het gebouw afhuren voor een feestje?
Neen. In principe houden we in JK Zomaar geen privé feestjes. Je kan natuurlijk steeds je verjaardag komen vieren, zowel op vrijdag als op andere dagen (naar gelang we voldoende barmensen vinden om op deze dagen open te houden). We vragen wel steeds van geen eigen drank mee te brengen. Indien je iets speciaal wilt (een vat bier, speciale cocktails, ...), willen we dit zeker aankopen.

### Ik ben iets kwijt!
Bekijk eens of het tussen onze [verloren voorwerpen](https://www.facebook.com/pg/jkzomaar/photos/?tab=album&album_id=10157002780767597) zit!

### Hoe word ik lid?
Je kan je lidkaart heel het jaar door kopen aan de kassa, voor slechts 5 euro! Een lidkaart is geldig tot het einde van het huidige werkjaar.

### Wat zijn de voordelen van een lidkaart?
Talloos! Op de jaarlijkse ledenfuif aan het begin van het werkjaar mag je meedrinken van het gratis vat, bij veel events mag je goedkoper of zelfs gratis binnen, je krijgt korting op merch, en je zal ook een vrijblijvende uitnodiging ontvangen voor de jaarlijkse crew-kickoff!
Het is ook een zeer effectieve manier om ons te steunen.

### Wat is een crewlid?
Een crewlid is de stap tussen lid en bestuur. Je mag mee openhouden, feestjes plannen, vergaderingen bijwonen, maar niets is verplicht!
Crewleden mogen ook mee op het jaarlijkse bestuursweekend. Je kan steeds intreden als crewlid, de basisvereiste is dat je lid bent van het huidig werkjaar, en dat je een barinitiatie hebt gekregen. De beste moment om dit te doen is tijdens de crew-kickoff aan het begin van elk werkjaar.
